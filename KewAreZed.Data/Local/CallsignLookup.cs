﻿using KewAreZed.Data.Upstream;

namespace KewAreZed.Data.Local;

public record CallsignLookup(Callsign? Callsign, ErrorType ErrorType);