﻿namespace KewAreZed.Data.Local;

public record Credentials(string Username, string Password)
{
    public string? SessionKey { get; set; }

    public Credentials(string username, string password, string sessionKey) : this(username, password)
    {
        SessionKey = sessionKey;
    }
}