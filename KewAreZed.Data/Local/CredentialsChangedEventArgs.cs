﻿namespace KewAreZed.Data.Local;

public class CredentialsChangedEventArgs : EventArgs
{
    public string Username { get; init; } = "";
    public string Password { get; init; } = "";
    public string? SessionKey { get; init; }
}