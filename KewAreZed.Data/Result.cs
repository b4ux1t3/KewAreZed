﻿using KewAreZed.Data.Upstream;

namespace KewAreZed.Data;

public record Result<T>(T? Value, ResultType Type)
{
    public bool WasSuccessful => Type is ResultType.Ok;
    public bool WasNotSuccessful => Type is ResultType.Error;
}

public enum ResultType{
    Ok,
    Error
}