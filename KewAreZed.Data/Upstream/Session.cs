﻿using System.Text.RegularExpressions;
using System.Xml.Serialization;

namespace KewAreZed.Data.Upstream;

public partial class Session
{
    [XmlElement("Key")] public string? Key { get; set; }
    [XmlElement("Count")] public int Count { get; set; }
    [XmlElement("SubExp")] public string? SubscriptionExpiration { get; set; }
    [XmlElement("GMTime")] public string? TimeStamp { get; set; }
    [XmlElement("Message")] public string? Message { get; set; }
    [XmlElement("Error")] public string? Error { get; set; }

    [XmlIgnore] public bool Errored => Error is not null;

    public ErrorType GetErrorType()
    {
        if (!Errored || Error is null) return ErrorType.None;
        if (Error == "Invalid session key") return ErrorType.InvalidSessionKey;
        if (Error.Trim() == "Username/password incorrect") return ErrorType.InvalidCredentials;
        return InvalidCallsignRegex().IsMatch(Error) ? ErrorType.InvalidCallsign : ErrorType.Other;
    }

    [GeneratedRegex("Not found:.*")]
    private static partial Regex InvalidCallsignRegex();
}

public enum ErrorType
{
    InvalidCredentials,
    InvalidSessionKey,
    InvalidCallsign,
    Other,
    None
}