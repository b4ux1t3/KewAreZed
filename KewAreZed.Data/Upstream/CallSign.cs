﻿using System.Xml.Serialization;

namespace KewAreZed.Data.Upstream;

public class Callsign
{
    /// <summary>
    /// callsign
    /// </summary>
    [XmlElement("call")] public string? Call { get; set; }
    /// <summary>
    /// Cross reference: the query callsign that returned this record
    /// </summary>
    [XmlElement("xref")] public string? Xref { get; set; }
    /// <summary>
    /// Other callsigns that resolve to this record
    /// </summary>
    [XmlElement("aliases")] public string? Aliases { get; set; }
    /// <summary>
    /// DXCC entity ID (country code) for the callsign
    /// </summary>
    [XmlElement("dxcc")] public string? DxccId { get; set; }
    /// <summary>
    /// first name
    /// </summary>
    [XmlElement("fname")] public string? FirstName { get; set; }
    /// <summary>
    /// last name
    /// </summary>
    [XmlElement("name")] public string? LastName { get; set; }
    /// <summary>
    /// address line 1 (i.e. house # and street)
    /// </summary>
    [XmlElement("addr1")] public string? AddressLine1 { get; set; }
    /// <summary>
    /// address line 2 (i.e, city name)
    /// </summary>
    [XmlElement("addr2")] public string? AddressLine2 { get; set; }
    /// <summary>
    /// state (USA Only)
    /// </summary>
    [XmlElement("state")] public string? State { get; set; }
    /// <summary>
    /// Zip/postal code
    /// </summary>
    [XmlElement("zip")] public string? Zip { get; set; }
    /// <summary>
    /// country name for the QSL mailing address
    /// </summary>
    [XmlElement("country")] public string? Country { get; set; }
    /// <summary>
    /// dxcc entity code for the mailing address country
    /// </summary>
    [XmlElement("ccode")] public string? CCode { get; set; }
    /// <summary>
    /// latitude of address (signed decimal)
    /// </summary>
    [XmlElement("lat")] public double Latitude { get; set; }
    /// <summary>
    /// longitude of address (signed decimal)
    /// </summary>
    [XmlElement("lon")] public double Longitude { get; set; }
    /// <summary>
    /// grid locator
    /// </summary>
    [XmlElement("grid")] public string? Grid { get; set; }
    /// <summary>
    /// county name (USA)
    /// </summary>
    [XmlElement("county")] public string? County { get; set; }
    /// <summary>
    /// FIPS county identifier (USA)
    /// </summary>
    [XmlElement("fips")] public string? Fips { get; set; }
    /// <summary>
    /// DXCC country name of the callsign
    /// </summary>
    [XmlElement("land")] public string? Land { get; set; }
    /// <summary>
    /// license effective date (USA)
    /// </summary>
    [XmlElement("efdate")] public string? LicenseEffectiveDate { get; set; }
    /// <summary>
    /// license expiration date (USA)
    /// </summary>
    [XmlElement("expdate")] public string? LicenseExpirationDate { get; set; }
    /// <summary>
    /// previous callsign
    /// </summary>
    [XmlElement("p_call")] public string? PreviousCall { get; set; }
    /// <summary>
    /// license class
    /// </summary>
    [XmlElement("class")] public string? Class { get; set; }
    /// <summary>
    /// license type codes (USA)
    /// </summary>
    [XmlElement("codes")] public string? Codes { get; set; }
    /// <summary>
    /// QSL manager info
    /// </summary>
    [XmlElement("qslmgr")] public string? QslManager { get; set; }
    /// <summary>
    /// email address
    /// </summary>
    [XmlElement("email")] public string? Email { get; set; }
    /// <summary>
    /// web page address
    /// </summary>
    [XmlElement("url")] public string? Url { get; set; }
    /// <summary>
    /// QRZ web page views
    /// </summary>
    [XmlElement("u_views")] public int UViews { get; set; }
    /// <summary>
    /// approximate length of the bio HTML in bytes
    /// </summary>
    [XmlElement("bio")] public int Bio { get; set; }
    /// <summary>
    /// date of the last bio update
    /// </summary>
    [XmlElement("biodate")] public string? LastBioUpdate { get; set; }
    /// <summary>
    /// full URL of the callsign's primary image
    /// </summary>
    [XmlElement("image")] public string? Image { get; set; }
    /// <summary>
    /// height:width:size in bytes, of the image file
    /// </summary>
    [XmlElement("imageinfo")] public string? ImageInfo { get; set; }
    /// <summary>
    /// QRZ db serial number
    /// </summary>
    [XmlElement("serial")] public string? Serial { get; set; }
    /// <summary>
    /// QRZ callsign last modified date
    /// </summary>
    [XmlElement("moddate")] public string? LastModified { get; set; }
    /// <summary>
    /// Metro Service Area (USPS)
    /// </summary>
    [XmlElement("MSA")] public string? Msa { get; set; }
    /// <summary>
    /// Telephone Area Code (USA)
    /// </summary>
    [XmlElement("AreaCode")] public string? AreaCode { get; set; }
    /// <summary>
    /// Time Zone (USA)
    /// </summary>
    [XmlElement("TimeZone")] public string? TimeZone { get; set; }
    /// <summary>
    /// GMT Time Offset
    /// </summary>
    [XmlElement("GMTOffset")] public string? GmtOffset { get; set; }
    /// <summary>
    /// Daylight Saving Time Observed
    /// </summary>
    [XmlElement("DST")] public string? Dst { get; set; }
    /// <summary>
    /// Will accept e-qsl (0/1 or blank if unknown)
    /// </summary>
    [XmlElement("eqsl")] public string? EQsl { get; set; }
    /// <summary>
    /// Will return paper QSL (0/1 or blank if unknown)
    /// </summary>
    [XmlElement("mqsl")] public string? MQsl { get; set; }
    /// <summary>
    /// CQ Zone identifier
    /// </summary>
    [XmlElement("cqzone")] public string? CqZone { get; set; }
    /// <summary>
    /// ITU Zone identifier
    /// </summary>
    [XmlElement("ituzone")] public string? ItuZone { get; set; }
    /// <summary>
    /// operator's year of birth
    /// </summary>
    [XmlElement("born")] public string? Born { get; set; }
    /// <summary>
    /// User who manages this callsign on QRZ
    /// </summary>
    [XmlElement("user")] public string? User { get; set; }
    /// <summary>
    /// Will accept LOTW (0/1 or blank if unknown)
    /// </summary>
    [XmlElement("lotw")] public string? Lotw { get; set; }
    /// <summary>
    /// IOTA Designator (blank if unknown)
    /// </summary>
    [XmlElement("iota")] public string? Iota { get; set; }
    /// <summary>
    /// Describes source of lat/long data
    /// </summary>
    [XmlElement("geoloc")] public string? GeoLoc { get; set; }
    /// <summary>
    /// Attention address line, this line should be prepended to the address
    /// </summary>
    [XmlElement("attn")] public string? Attn { get; set; }
    /// <summary>
    /// A different or shortened name used on the air
    /// </summary>
    [XmlElement("nickname")] public string? Nickname { get; set; }
    /// <summary>
    /// Combined full name and nickname in the format used by QRZ. This format is subject to change.
    /// </summary>
    [XmlElement("name_fmt")] public string? NameFormat { get; set; }
}