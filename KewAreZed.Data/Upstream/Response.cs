﻿using System.Xml.Serialization;

namespace KewAreZed.Data.Upstream;

[XmlRoot(ElementName = "QRZDatabase", Namespace = "http://xmldata.qrz.com")]
public class Response
{
    [XmlAttribute("version")] public string? Version { get; set; }
    [XmlElement("Session")] public Session? Session { get; set; }
    [XmlElement("Callsign")] public Callsign? Callsign { get; set; }
    [XmlElement("DXCC")] public Dxcc? Dxcc { get; set; }
    
    public bool HasErrors => Session?.Errored ?? true;
}