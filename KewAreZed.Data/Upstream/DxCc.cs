﻿using System.Xml.Serialization;

namespace KewAreZed.Data.Upstream;

public class Dxcc
{
    /// <summary>
    /// DXCC entity number for this record
    /// </summary>
    [XmlElement("dxcc")] public string DxccId { get; set; }
    /// <summary>
    /// 2-letter country code (ISO-3166)
    /// </summary>
    [XmlElement("cc")] public string ShortCountryCode { get; set; }
    /// <summary>
    /// 3-letter country code (ISO-3166)
    /// </summary>
    [XmlElement("ccx")] public string LongCountryCode { get; set; }
    /// <summary>
    /// long name
    /// </summary>
    [XmlElement("name")] public string Name { get; set; }
    /// <summary>
    /// 2-letter continent designator
    /// </summary>
    [XmlElement("continent")] public string Continent { get; set; }
    /// <summary>
    /// ITU Zone
    /// </summary>
    [XmlElement("ituzone")] public string ItuZone { get; set; }
    /// <summary>
    /// CQ Zone
    /// </summary>
    [XmlElement("cqzone")] public string CqZone { get; set; }
    /// <summary>
    /// UTC timezone offset +/-
    /// </summary>
    [XmlElement("timezone")] public string Timezone { get; set; }
    /// <summary>
    /// Latitude (approx.)
    /// </summary>
    [XmlElement("lat")] public string Lat { get; set; }
    /// <summary>
    /// Longitude (approx.)
    /// </summary>
    [XmlElement("lon")] public string Lon { get; set; }
    /// <summary>
    /// Special notes and/or exceptions
    /// </summary>
    [XmlElement("notes")] public string Notes { get; set; }
}