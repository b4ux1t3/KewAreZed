﻿using System.Text;
using KewAreZed.API;
using KewAreZed.Data;
using KewAreZed.Data.Local;
using McMaster.Extensions.CommandLineUtils;
using Microsoft.Extensions.Configuration;

namespace KewAreZed.Cli;

internal class Program
{
    private static string ConfigLocation => $"{Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)}{Path.DirectorySeparatorChar}.kewarezed";
    
    [Option("-c|--config")]
    private string ConfigFilePath { get; } =
        $"{ConfigLocation}{Path.DirectorySeparatorChar}config.json";

    [Option("-u|--username")] private string? Username { get; } = string.Empty;
    [Option("-p|--password")] private bool PasswordProvided { get; } = false;
    [Option("-cs|--callsign")] private string? Callsign { get; } = string.Empty;
    [Option("-dx|--dxcc")] private string? DxccId { get; } = string.Empty;

    private Credentials? _creds;
    private IConfiguration? _config;

    private KewAreZedClient? _client;
    
    public static async Task<int> Main(string[] args) => await CommandLineApplication.ExecuteAsync<Program>(args);
    // ReSharper disable once UnusedMember.Local
    private async Task<int> OnExecute()
    {
        _handlePasswordInput();
        _initializeConfig();
        if (_config is not null)
            _creds ??= new Credentials(
                _config["Credentials:Username"] ?? "",
                _config["Credentials:Password"] ?? ""
            );
        if (
            _creds is null
            || string.IsNullOrWhiteSpace(_creds.Username)
            || string.IsNullOrWhiteSpace(_creds.Password)
        )
        {
            await Console.Error.WriteLineAsync("Could not establish credentials. Please run:");
            await Console.Error.WriteLineAsync($"{System.Diagnostics.Process.GetCurrentProcess().ProcessName} -u <username> -p");
            return 1;
        }
        
        _client = new KewAreZedClient(_creds);

        await _client.Login();
        await _testCallsign();
        await _testDxcc();

        return 0;
    }

    private void _handlePasswordInput()
    {
        if (!PasswordProvided) return;
        string password;
        if (!string.IsNullOrWhiteSpace(Username)) password = _getPassword();
        else
        {
            Console.Error.WriteLine("Please pass a username with -u when you use the -p.");
            return;
        }

        _creds = new Credentials(Username, password);
        if (!Directory.Exists(ConfigLocation))
        {
            Directory.CreateDirectory(ConfigLocation);
        }
        File.WriteAllText(ConfigFilePath, System.Text.Json.JsonSerializer.Serialize(new ConfigurationFile(_creds)));
    }

    private string _getPassword()
    {
        Console.Write("Password: ");
        var password = new StringBuilder();
        var exit = false;
        var key = Console.ReadKey(true);
        exit = key.Key is ConsoleKey.Enter;
        while (!exit)
        {
            password.Append(key.KeyChar);
            key = Console.ReadKey(true);
            exit = key.Key is ConsoleKey.Enter;
        }

        return password.ToString();
    }
    
    private void _initializeConfig()
    {
        var configBuilder = new ConfigurationBuilder();
        if (File.Exists(ConfigFilePath)) configBuilder.AddJsonFile(ConfigFilePath, optional: true);
        _config = configBuilder.Build();
    }

    private async Task _testCallsign()
    {
        if (Callsign is null || _client is null) return;
        var result = await _client.LookupCallsign(Callsign);
        if (result.Type is ResultType.Error) return;
        Console.WriteLine(result.Value);
    }
    private async Task _testDxcc()
    {
        if (DxccId is null || _client is null) return;
        var result = await _client.LookupDxcc(DxccId);
        if (result is null) return;
        Console.WriteLine(result.Name);
    }
}