﻿using KewAreZed.Data.Local;

namespace KewAreZed.Cli;

public record ConfigurationFile(Credentials Credentials);