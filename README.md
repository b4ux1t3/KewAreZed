# KewAreZed

KewAreZed is primarily a .NET abstraction over top of [QRZ's](https://www.qrz.com/) [XML API](https://www.qrz.com/XML/current_spec.html). It is also a .NET MAUI-based application for logging your contacts, which acts as an example implementation of the API.
