﻿namespace KewAreZed.API;

public class CredentialsInvalidException : Exception
{
    public CredentialsInvalidException() : base("The credentials stored are no longer valid")
    {
        
    }
}