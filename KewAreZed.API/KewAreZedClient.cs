﻿using System.Diagnostics;
using System.Net.Http.Headers;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using KewAreZed.Data;
using KewAreZed.Data.Local;
using KewAreZed.Data.Upstream;

namespace KewAreZed.API;

public class KewAreZedClient
{
    private Credentials? _loginCredentials;
    private string _sessionKey = "";

    private readonly HttpClient _http;

    private static readonly ProductInfoHeaderValue UserAgent = new("b4ux1t3_dotnet_QRZ_API", "0.1");
    private readonly XmlSerializer _serializer = new (typeof(Response));
    public event EventHandler? CredentialsChanged;
    public delegate void CredentialsChangedEventHandler(object sender, CredentialsChangedEventArgs e);

    public KewAreZedClient(Credentials loginCreds)
    {
        _loginCredentials = loginCreds;
        _http = new HttpClient();
        _http.BaseAddress = new Uri("https://xmldata.qrz.com/xml/current/");
        _http.DefaultRequestHeaders.UserAgent.Add(UserAgent);
    }

    public KewAreZedClient()
    {
        _http = new HttpClient();
        _http.BaseAddress = new Uri("https://xmldata.qrz.com/xml/current/");
        _http.DefaultRequestHeaders.UserAgent.Add(UserAgent);
    }

    public async Task Login()
    {
        await _logInWithCredentials();
    }

    public async Task<bool> Login(string username, string password)
    {
        _loginCredentials = new (username, password);
        return await _logInWithCredentials();
    }

    public async Task<Result<CallsignLookup>> LookupCallsign(string callsign) => await _getCallsign(callsign);
    public async Task<Dxcc?> LookupDxcc(string id) => await _getDxcc(id);
    public async Task<Dxcc?> LookupDxcc(int id) => await _getDxcc(id);

    private static bool _responseHasErrors(Response resp) => resp.Session?.Errored ?? true;

    private async Task<XmlDocument> _makeApiRequest(string request)
    {
        var res = await _http.GetAsync(request);
        try
        {
            res.EnsureSuccessStatusCode();
        }
        catch (HttpRequestException ex)
        {
            throw new InvalidOperationException(ex.Message);
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex);
        }
        var doc = new XmlDocument();
        var str = await res.Content.ReadAsStringAsync();
        var st = new MemoryStream(Encoding.UTF8.GetBytes(str));
        doc.Load(st);

        return doc;
    }

    private async Task<Response> _sendRequest(string requestUrl)
    {
        var responseDoc = await _makeApiRequest(requestUrl);
        using var reader = new XmlNodeReader(responseDoc);
        return (Response) (_serializer.Deserialize(reader) ?? new Response());
    }

    private async Task<bool> _logInWithCredentials()
    {
        if (_loginCredentials is null) throw new InvalidOperationException($"We should have {nameof(_loginCredentials)}! Use Login first.");
        var requestUrl = $"?username={_loginCredentials.Username}&password={_loginCredentials.Password}&agent={UserAgent}";
        var response = await _sendRequest(requestUrl);
        if (_responseHasErrors(response)) throw new InvalidOperationException(response.Session?.Error ?? "Invalid response object");
        if (response.Session is not null && !string.IsNullOrWhiteSpace(response.Session.Key)) 
            CredentialsChanged?.Invoke(
                this, 
                new CredentialsChangedEventArgs
                {
                    Username = _loginCredentials.Username,
                    Password =_loginCredentials.Password,
                    SessionKey = response.Session?.Key ?? null
                });
        
        _sessionKey = response.Session?.Key ?? "";
        
        return !string.IsNullOrWhiteSpace(_sessionKey);
    }

    private async Task<Result<CallsignLookup>> _getCallsign(string callsign)
    {
        var requestUrl = $"?s={_sessionKey}&callsign={callsign}";
        var response = await _sendRequest(requestUrl);
        if (response.Session is null) throw new ("No session was attached to the response.");
        switch (response.Session.GetErrorType())
        {
            case ErrorType.InvalidCredentials:
                throw new CredentialsInvalidException();
            // If we see this error, we've probably got an expired session. Login and try again 
            case ErrorType.InvalidSessionKey:
                await _logInWithCredentials(); 
                await _getCallsign(callsign);
                break;
            case ErrorType.InvalidCallsign:
            case ErrorType.None:
                // In either of these cases, we can just pass things along as they are.
                return new Result<CallsignLookup>(
                    new(response.Callsign, response.Session.GetErrorType()), 
                    ResultType.Ok);
                
            case ErrorType.Other:
                throw new InvalidOperationException("An unknown error occurred");
            default:
                throw new ArgumentOutOfRangeException();
        }

        throw new UnreachableException();
    }

    private async Task<Dxcc?> _getDxcc(int id) => await _getDxcc(id.ToString());
    private async Task<Dxcc?> _getDxcc(string id)
    {
        var requestUrl = $"?s={_sessionKey}&dxcc={id}";
        var response = await _sendRequest(requestUrl);
        if (!_responseHasErrors(response)) return response.Dxcc;
        await _logInWithCredentials(); // If we see an error, we've probably got an expired session. Login and try again 
        return await _getDxcc(id);
    }

    public async Task<bool> TestCredentials(Credentials credentials)
    {
        if (credentials.SessionKey is null) return await Login(credentials.Username, credentials.Password);
        var result = await _sendRequest($"?s={_sessionKey}&callsign=INVALID CALL");
        if (result.Session?.GetErrorType() is ErrorType.InvalidCallsign) return true;

        return await Login(credentials.Username, credentials.Password);
    }
}