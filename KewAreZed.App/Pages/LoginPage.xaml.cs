﻿using KewAreZed.App.ViewModels;

namespace KewAreZed.App.Pages;

public partial class LoginPage : ContentPage
{

	public LoginPage(LoginPageViewModel viewModel)
	{
		InitializeComponent();
		BindingContext = viewModel;
	}

	private void _onUsernameChanged(object sender, TextChangedEventArgs e)
	{
		((LoginPageViewModel) BindingContext).OnUsernameChanged(((Entry) sender).Text);
	}
    
	private void _onPasswordChanged(object sender, TextChangedEventArgs e){
		((LoginPageViewModel) BindingContext).OnPasswordChanged(((Entry) sender).Text);
	}
    
}

