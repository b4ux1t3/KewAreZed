﻿using KewAreZed.App.ViewModels;

namespace KewAreZed.App.Pages;

public partial class CallsignLookupPage : ContentPage
{

    public CallsignLookupPage(CallsignLookupPageViewModel viewModel)
    {
        InitializeComponent();
        BindingContext = viewModel;
    }
    
    private void _onSearchChanged(object sender, TextChangedEventArgs e)
    {
        ((CallsignLookupPageViewModel) BindingContext).OnSearchChanged(((Entry) sender).Text);
    }
}