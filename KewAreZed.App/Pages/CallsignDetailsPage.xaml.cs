﻿using KewAreZed.App.ViewModels;

namespace KewAreZed.App.Pages;

public partial class CallsignDetailsPage : ContentPage
{
    public CallsignDetailsPage(CallsignViewModel viewModel)
    {
        InitializeComponent();

        BindingContext = viewModel;
    }
    
}