﻿using KewAreZed.App.Pages;
using KewAreZed.App.Services;

namespace KewAreZed.App;

public partial class App : Application
{
	private readonly StoredCredentialManager _storedCredentialManager;
	private bool _couldLogIn = false;
	public App(StoredCredentialManager storedCredentialManager)
	{
		_storedCredentialManager = storedCredentialManager;
		InitializeComponent();

		MainPage = new AppShell();
	}
	
	protected override void OnStart()
	{
		var task = _initAsync();

		task.ContinueWith(_ =>
		{
			Current?.Dispatcher.Dispatch(() =>
			{
				if (_couldLogIn) Shell.Current.GoToAsync($"{nameof(CallsignLookupPage)}");
				MainPage = new AppShell();
				Shell.Current.GoToAsync($"{nameof(LoginPage)}");
			});
		});

		base.OnStart();
	}

	private async Task _initAsync()
	{
		_couldLogIn = await _storedCredentialManager.LoadCredentials();
	}
}
