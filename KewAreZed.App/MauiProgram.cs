﻿using CommunityToolkit.Maui;
using KewAreZed.API;
using KewAreZed.App.Pages;
using KewAreZed.App.Services;
using KewAreZed.App.ViewModels;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging;

namespace KewAreZed.App;

public static class MauiProgram
{
	private static readonly string ConfigLocation = 
		$"{Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)}{Path.DirectorySeparatorChar}.kewarezed";
	private static readonly string ConfigFilePath =
		$"{ConfigLocation}{Path.DirectorySeparatorChar}config.json";

	public static MauiApp CreateMauiApp()
	{
		var builder = MauiApp.CreateBuilder();
		builder
			.UseMauiApp<App>()
			.UseMauiCommunityToolkit()
			.ConfigureFonts(fonts =>
			{
				fonts.AddFont("Assets/fira-sans-v16-latin-regular.ttf", "FiraSansRegular");
				fonts.AddFont("Assets/FiraSans-Bold.ttf", "FiraSansBold");
			})
			.UseMauiMaps();
					
		var services = builder.Services;
		
		/* Services */
		services.TryAddSingleton<KewAreZedClient>();
		services.TryAddSingleton<StoredCredentialManager>();
		
		/* View Models */
		services.TryAddSingleton<CallsignLookupPageViewModel>();
		services.TryAddSingleton<CallsignViewModel>();
		services.TryAddSingleton<LoginPageViewModel>();
		
		/* Pages */
		services.TryAddSingleton<LoginPage>();
		services.TryAddSingleton<CallsignLookupPage>();
		services.TryAddTransient<CallsignDetailsPage>();
		

#if DEBUG
		builder.Logging.AddDebug();
#endif

		return builder.Build();
	}
}
