﻿using KewAreZed.API;
using KewAreZed.Data.Local;

namespace KewAreZed.App.Services;

public class StoredCredentialManager
{
    private Credentials? Credentials { get; set; }
    private readonly KewAreZedClient _client;
    
    public StoredCredentialManager(KewAreZedClient client)
    {
        _client = client;
        client.CredentialsChanged += _onCredentialsChanged;
    }

    private void _onCredentialsChanged(object? _, EventArgs e)
    {
        var castE = (CredentialsChangedEventArgs) e;
        Credentials = new Credentials(castE.Username, castE.Password, castE.SessionKey ?? "");
    }

    public async Task<bool> LoadCredentials()
    {
        var username = await SecureStorage.Default.GetAsync("username");
        if (string.IsNullOrWhiteSpace(username)) return false;
        
        var password = await SecureStorage.Default.GetAsync("password");
        if (string.IsNullOrWhiteSpace(password)) return false;
        
        var sessionKey = await SecureStorage.Default.GetAsync("sessionKey");

        Credentials = new(username, password, sessionKey);
        
        return await _client.TestCredentials(Credentials);
    }

    public async Task WriteCredentials()
    {
        if (Credentials is null) return;
        await SecureStorage.Default.SetAsync("username", Credentials.Username);
        await SecureStorage.Default.SetAsync("password", Credentials.Password);
        if (Credentials.SessionKey is not null) await SecureStorage.Default.SetAsync("sessionKey", Credentials.SessionKey);
    }
}