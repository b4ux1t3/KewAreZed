﻿using KewAreZed.App.Pages;

namespace KewAreZed.App;

public partial class AppShell : Shell
{
	public AppShell()
	{
		InitializeComponent();
		
		Routing.RegisterRoute(nameof(LoginPage), typeof(LoginPage));
		Routing.RegisterRoute(nameof(CallsignLookupPage), typeof(CallsignLookupPage));
		Routing.RegisterRoute(nameof(CallsignDetailsPage), typeof(CallsignDetailsPage));
	}
}
