﻿using CommunityToolkit.Mvvm.ComponentModel;
using KewAreZed.Data.Upstream;
using Microsoft.Maui.Maps;

namespace KewAreZed.App.ViewModels;

[QueryProperty("Callsign", "Callsign")]
public partial class CallsignViewModel : BaseViewModel
{
    
    [ObservableProperty]
    private Callsign _callsign = new Callsign { Call = "<INVALID>" };

    public MapSpan Area => new(
        new Location(Callsign.Latitude, Callsign.Longitude),
        0.1,
        0.1);
}