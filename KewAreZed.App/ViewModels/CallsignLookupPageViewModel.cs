﻿using System.Diagnostics;
using CommunityToolkit.Mvvm.Input;
using KewAreZed.API;
using KewAreZed.App.Pages;
using KewAreZed.Data.Upstream;

namespace KewAreZed.App.ViewModels;

public partial class CallsignLookupPageViewModel : BaseViewModel
{
    private readonly KewAreZedClient _apiService;

    private string _lookupText = "";

    public bool EnableSearchButton => !string.IsNullOrWhiteSpace(_lookupText) && IsNotBusy;
    
    public CallsignLookupPageViewModel(KewAreZedClient apiService)
    {
        Title = "Callsign Lookup";
        _apiService = apiService;
    }
    
    [RelayCommand]
    private async Task _lookupCallsign()
    {
        Callsign? callInfo = null; 
        try
        {
            IsBusy = true;
            
            var res = await _apiService.LookupCallsign(_lookupText);
            if (res is {WasSuccessful: true, Value: { }})
            {
                switch (res.Value.ErrorType)
                {
                    case ErrorType.InvalidCallsign:
                        await _displayError("Invalid Callsign!");
                        break;
                    case ErrorType.None:
                        callInfo = res.Value.Callsign;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }
        catch (Exception ex)
        {
            Debug.WriteLine(ex.Message);
        }
        finally
        {
            IsBusy = false;
        }

        if (callInfo is null) return;
        // Bring up a new page with the callsign.
        await Shell.Current.GoToAsync(
            $"{nameof(CallsignDetailsPage)}", 
            new Dictionary<string, object>
            {
                {"Callsign", callInfo}
            });
    }
    
    public void OnSearchChanged(string text)
    {
        _lookupText = text;
    }

    private async Task _displayError(string message)
    {
        // CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
        //
        // var snackbarOptions = new SnackbarOptions
        // {
        //     CornerRadius = new CornerRadius(10),
        //     Font = Font.SystemFontOfSize(14),
        //     ActionButtonFont = Font.SystemFontOfSize(14),
        //     CharacterSpacing = 0.5
        // };
        //
        // async void Action() => await Application.Current?.MainPage?.DisplayAlert("Error", message, "OK")!;
        // TimeSpan duration = TimeSpan.FromSeconds(3);
        //
        // var snackbar = Snackbar.Make(message, null, "OK", duration, snackbarOptions);
        //
        // await snackbar.Show(cancellationTokenSource.Token);
        
        await Application.Current?.MainPage?.DisplayAlert("Error", message, "OK")!;
    }
}