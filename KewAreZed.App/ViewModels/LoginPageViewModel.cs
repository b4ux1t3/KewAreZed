﻿using System.Diagnostics;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using KewAreZed.API;
using KewAreZed.App.Pages;
using KewAreZed.App.Services;

namespace KewAreZed.App.ViewModels;

public partial class LoginPageViewModel : BaseViewModel
{
    private readonly KewAreZedClient _apiService;
    public StoredCredentialManager StoredCredentialManager { get; }

    private string _username = "";
    private string _password = "";

    [ObservableProperty]
    private bool _triedToLogIn;

    [ObservableProperty]
    [NotifyPropertyChangedFor(nameof(LoginUnsuccessful))]
    private bool _loginSuccessful;

    public bool LoginUnsuccessful => !LoginSuccessful;
    public bool ShowUnsuccessfulLogin => TriedToLogIn && LoginUnsuccessful;
    
    public LoginPageViewModel(KewAreZedClient apiService, StoredCredentialManager storedCredentialManager)
    {
        _apiService = apiService;
        StoredCredentialManager = storedCredentialManager;
        Title = "Login";
    }
    
    [RelayCommand]
    private async Task _login()
    {
        try
        {
            IsBusy = true;
            LoginSuccessful = await _apiService.Login(_username, _password);
        }
        catch (Exception ex)
        {
            Debug.WriteLine(ex.Message);
        }
        finally
        {
            IsBusy = false;
            TriedToLogIn = true;
        }

        if (!LoginSuccessful) return;
        await StoredCredentialManager.WriteCredentials();

        await Shell.Current.GoToAsync($"//{nameof(CallsignLookupPage)}");
    }
    
    public void OnUsernameChanged(string text)
    {
        _username = text;
    }
    public void OnPasswordChanged(string text){
        _password = text;
    }
    
}